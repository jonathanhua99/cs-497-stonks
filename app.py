#required to pip install tensorflow, flask, keras, yfinance, lxml, sklearn
#python 3.7 or lower to function with tensorflow & keras dependencies
from flask import Flask, render_template, url_for, request
import pandas as pd
import numpy as np
import pandas_datareader as web
from tensorflow.keras.callbacks import EarlyStopping
from tensorflow.python.keras.models import Sequential 
from tensorflow.python.keras.layers import LSTM, Dense
from sklearn.preprocessing import MinMaxScaler
import yfinance as yf 
import base64
import io
import math
import matplotlib.pyplot as plt
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.figure import Figure
import datetime as dt

app = Flask(__name__)

def calculatePrediction(tickerSymbol):

    DATE_START = '2012-01-01'
    DATE_END = '2020-05-08' #str(dt.datetime.now())
    DAY_WINDOW = 60 
    DAYS_IN_ADV = 30 
    user_input = tickerSymbol
    data = web.DataReader(user_input, data_source = 'yahoo', start = DATE_START, end = DATE_END)
    closeData = data.filter(['Close'])
    dataset = closeData.values
    training_data_len = math.ceil(len(dataset) * .8)
    training_data_len
    scaler = MinMaxScaler(feature_range = (0,1))
    scaled_data = scaler.fit_transform(dataset)
    train_data = scaled_data[0 : training_data_len, :]
    x_train = []
    y_train = []
    
    for i in range(DAY_WINDOW, len(train_data)):
        x_train.append(train_data[i - DAY_WINDOW : i, 0])
        y_train.append(train_data[i, 0])
    x_train, y_train = np.array(x_train), np.array(y_train)
    x_train = np.reshape(x_train, (x_train.shape[0], x_train.shape[1], 1))
    x_train.shape

    #Create test data
    test_data = scaled_data[training_data_len - DAY_WINDOW:, :]
    x_test = []
    y_test = dataset[training_data_len:, :] #These are unscaled

    for i in range(DAY_WINDOW, len(test_data)):
        x_test.append(test_data[i - DAY_WINDOW : i, 0])

    x_test = np.array(x_test)
    x_test = np.reshape(x_test, (x_test.shape[0], x_test.shape[1], 1))

    #Provide input shape and callback
    LSTMshape = x_train.shape[1], 1
    callback = EarlyStopping(monitor = 'loss', patience = 2)

    #Building model
    model = Sequential()
    model.add(LSTM(50, return_sequences = True, input_shape = (LSTMshape)))
    model.add(LSTM(50, return_sequences = False))
    model.add(Dense(25))
    model.add(Dense(1))

    model.compile(optimizer = 'adam', loss = 'mean_squared_error')
    training = model.fit(x_train, y_train, batch_size = 4, epochs = 5, callbacks = [callback])

    #Get model predictions / rmse stats
    predictions = model.predict(x_test)
    predictions = scaler.inverse_transform(predictions)

    rmse = np.sqrt(np.mean((predictions - y_test)**2))
    print('\n RMSE: ', rmse, '\n')
    
    fig=Figure(figsize=(20,20))
    axis = fig.add_subplot(2, 2, 1)
    axis.set_title("Actual Closing Prices")
    axis.set_xlabel("Dates")
    axis.set_ylabel("Close Price USD($)")
    axis.plot(data["Close"])

    ###return pngImageB64String1

    #Configure the data
    train = closeData[:training_data_len]
    valid = closeData[training_data_len:]
    valid['Predictions'] = predictions

    axis2 = fig.add_subplot(2, 2, 2)
    axis2.set_title("Predicted")
    axis2.set_xlabel("Dates")
    axis2.set_ylabel("Close Price USD($)")
    axis2.plot(train["Close"], color='red')
    axis2.plot(valid['Close'], color = 'green')
    axis2.plot(valid['Predictions'], color = 'orange')
    axis2.legend(['Training Set', 'Test Set', 'Predictions'], loc = 'upper left')

    #Plot MSE
    axis3 = fig.add_subplot(2,2,3)
    axis3.set_title('Mean Square Error Loss')
    axis3.set_xlabel('Epochs', fontsize = 20)
    axis3.set_ylabel('Square Difference between Actual and Predicted Values', fontsize = 14)
    axis3.plot(training.history['loss'], label='loss', color = 'blue')

    newdf = data.filter(['Close'])

    # define index for next X days
    last_date = data.iloc[-1].name
    modified_date = last_date + dt.timedelta(days=1)
    new_date = pd.date_range(modified_date , periods = DAYS_IN_ADV, freq='D')

    column_names = ['Close']
    daysDf = pd.DataFrame(columns = column_names)
    dfList = []

    for x in range(DAYS_IN_ADV):
    #Fit the data based on the last 45 days
        previous_X_days = newdf[-DAY_WINDOW:].values #45 days gives the best results in the window
        previous_X_scaled = scaler.transform(previous_X_days)
        X_test = []
        X_test.append(previous_X_scaled)
        X_test = np.array(X_test)
        X_test = np.reshape(X_test, (X_test.shape[0], X_test.shape[1], 1))
        pred_price = model.predict(X_test)
        pred_price = scaler.inverse_transform(pred_price)

        pred_price = np.reshape(pred_price, (pred_price.shape[0]))
        forecasted = pd.DataFrame(pred_price, columns=['Close'])
        #Append to newdf to run fit again on the added prediction value
        newdf = newdf.append(forecasted)
        #Append to new dataframe to display in the charts
        daysDf = daysDf.append(forecasted)
        pred_price.astype(int)
        dfList.append(pred_price[:x + 1])


    x = len(dfList)
    dfList = np.array(dfList)
    finalDf = pd.DataFrame(dfList, columns=['Close'], index = new_date)

    useData = web.DataReader(user_input, data_source = 'yahoo', start = '2019-12-01', end = DATE_END)
    axis4 = fig.add_subplot(2,2,4)
    axis4.set_title('Predicted Prices for X days')
    axis4.set_xlabel('Date', fontsize = 20)
    axis4.set_ylabel('Close Price $ USD', fontsize = 20)
    axis4.plot(useData['Close'], color = 'blue')
    axis4.plot(finalDf['Close'], color = 'orange')
    axis4.legend(['Actual Data', 'Predicted Data'], loc = 'upper left')

    #process the image to the byte string and return
    pngImage = io.BytesIO()
    FigureCanvas(fig).print_png(pngImage)
    pngImageB64String = "data:image/png;base64,"
    pngImageB64String += base64.b64encode(pngImage.getvalue()).decode('utf8')

    #process text portion of the prediction 
    predictionString = finalDf.to_html()

    return pngImageB64String, predictionString


@app.route('/')
def index():
    return render_template('index.html')

@app.route('/process_ticker', methods=['GET', 'POST'])
def process_ticker():
    if request.method == 'POST':
        tickerInput = request.form['tickerInput'].upper()
        try:
            tickerObj = yf.Ticker(tickerInput)
            companyDescription = tickerObj.info["longBusinessSummary"]
            longName = tickerObj.info["longName"]
            sector = tickerObj.info["sector"]
            industry = tickerObj.info["industry"]
            website = tickerObj.info["website"]
            logo_url = tickerObj.info["logo_url"]
        except:
            companyDescription = "Couldn't find the description through yfinance api."
            longName = "Couldn't find the name through yfinance api."
            sector = "Couldn't find the sector through yfinance api."
            industry = "Couldn't find the industry through yfinance api."
            website = "Couldn't find the website through yfinance api."
            logo_url = "Couldn't find the logo through yfinance api."
        plot_url, predictionString = calculatePrediction(tickerInput)


    return render_template('ticker_calculation.html', 
        tickerInput = tickerInput, 
        companyDescription = companyDescription,
        longName = longName,
        sector = sector,
        industry = industry,
        website = website,
        logo_url = logo_url,
        plot_url = plot_url,
        predictionString = predictionString)

if __name__ == "__main__":
    app.run(debug=True)