#New ReadME as of 5/11/2020# 

Repository Project for CS497-2 Final 

This project is a basic flask web server that runs on your local host at port 5000. It utilizes the yfinance API to retrieve stock information, as well as historical data. 
On the basic web interface, you will be able to search for a stock by it's stock ticker. After retrieving your stock's basic information, the script will then create a LSTM 
RNN to predict the future stock price. 

Notes:
-ensure that your python path is configured properly to run the file named "app.python". 
-python 3.7 or lower to work with tensorflow, tensorflow2 has issues running with python 3.8
-install dependencies using pip install such as tensorflow, keras, flask, pandas, numpy, yahoo finance, matplotlib, pandas_datareader, lxml
-some stock tickers will be unable to have basic information retrieved for them, but the stock graphs will still show up 
-